<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

  <style>
    body {
      background-image: url({{ asset('frontend/pattern.jpg') }});
    }
  </style>

  <title>SANBERCODE | Laravel Dasar</title>
</head>

<body>

  <div class="container d-flex p-4 justify-content-center">
    <div class="card border-0">
      <img src="{{ asset('frontend/logo.png') }}" class="card-img-top" alt="">
      <div class="card-body">
        <h2 class="card-text">Social Media Developer Santai Berkualitas</h2>
        <p class="card-text">
          Belajar dan Berbagi agar hidup ini semakin santai Berkualitas
          <br>
          <h3><span class="badge badge-primary">
              Benefit Join di SanberBook
            </span>
          </h3>
          <br>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Mendapatkan motivasi dari sesama developer</li>
            <li class="list-group-item">Sharing knowledge dari para mastah Sanber</li>
            <li class="list-group-item">Dibuat oleh calon web developer terbaik</li>
          </ul>

          <br>
          <h3><span class="badge badge-primary">
              Cara Bergabung ke SanberBook
            </span>
          </h3>
          <br>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Mengunjungi Website ini</li>
            <li class="list-group-item">Mendaftar di <h4><a href="{{ route('register') }}" class="badge badge-success">Form Sign Up</a></h4></li>
            <li class="list-group-item">Selesai!</li>
          </ul>
        </p>
      </div>
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>